# Flutter app
An IoF app with a touch of A.I

## Install Flutter and Dart
In IDE : File->Settings->Plugins->Download and install flutter and dart
Deal with issues until doctor is ready

## Checking the setup
flutter doctor

## Emulator
Create a Pixel C tablet device with Oreo - API Level 27 - Android 8.1 - Google APIS

## Run flutter app
Launch emulator
flutter run

