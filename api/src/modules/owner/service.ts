import { Service } from "typedi";
import { ObjectId } from "mongodb";

import OwnerModel from "./model";
import { Owner } from "../../entities/Owner";
import { NewOwnerInput } from "./input";

@Service() // Dependencies injection
export default class OwnerService {
  constructor(private readonly OwnerModel: OwnerModel) {}

  public async getById(_id: ObjectId): Promise<Owner | null> {
    return this.OwnerModel.getById(_id);
  }

  public async addOwner(data: NewOwnerInput): Promise<Owner> {
    const newOwner = await this.OwnerModel.create(data);

    // Business logic goes here
    // Example:
    // Trigger push notification, analytics, ...

    return newOwner;
  }
}
