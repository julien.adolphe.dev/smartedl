import { getModelForClass } from "@typegoose/typegoose";
import { ObjectId } from "mongodb";

import { Owner } from "../../entities/Owner";
import { NewOwnerInput } from "./input";

// This generates the mongoose model for us
export const OwnerMongooseModel = getModelForClass(Owner);

export default class OwnerModel {
  async getById(_id: ObjectId): Promise<Owner | null> {
    // Use mongoose as usual
    return OwnerMongooseModel.findById(_id).lean().exec();
  }

  async create(data: NewOwnerInput): Promise<Owner> {
    const Owner = new OwnerMongooseModel(data);
    await Owner.save();
    return Owner;
  }
}
