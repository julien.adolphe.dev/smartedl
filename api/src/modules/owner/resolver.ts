import { Resolver, Arg, Query, Mutation, ID } from "type-graphql";
import { Service } from "typedi";
import { ObjectId } from "mongodb";

import { Owner } from "../../entities/Owner";
import OwnerService from "./service";
import { NewOwnerInput } from "./input";

/*
  IMPORTANT: Your business logic must be in the service!
*/

@Service() // Dependencies injection
@Resolver((of) => Owner)
export default class OwnerResolver {
  constructor(private readonly OwnerService: OwnerService) {}

  @Query((returns) => Owner)
  async getOwner(@Arg("id") id: ObjectId) {
    const Owner = await this.OwnerService.getById(id);

    return Owner;
  }

  @Mutation((returns) => Owner)
  async createOwner(
    @Arg("createOwnerData") createOwnerData: NewOwnerInput
  ): Promise<Owner> {
    const Owner = await this.OwnerService.addOwner(createOwnerData);
    return Owner;
  }
}