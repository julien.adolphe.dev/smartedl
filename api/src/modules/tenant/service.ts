import { Service } from "typedi";
import { ObjectId } from "mongodb";

import TenantModel from "./model";
import { Tenant } from "../../entities/Tenant";
import { NewTenantInput } from "./input";

@Service() // Dependencies injection
export default class TenantService {
  constructor(private readonly TenantModel: TenantModel) {}

  public async getById(_id: ObjectId): Promise<Tenant | null> {
    return this.TenantModel.getById(_id);
  }

  public async addTenant(data: NewTenantInput): Promise<Tenant> {
    const newTenant = await this.TenantModel.create(data);

    // Business logic goes here
    // Example:
    // Trigger push notification, analytics, ...

    return newTenant;
  }
}
