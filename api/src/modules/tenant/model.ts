import { getModelForClass } from "@typegoose/typegoose";
import { ObjectId } from "mongodb";

import { Tenant } from "../../entities/Tenant";
import { NewTenantInput } from "./input";

// This generates the mongoose model for us
export const TenantMongooseModel = getModelForClass(Tenant);

export default class TenantModel {
  async getById(_id: ObjectId): Promise<Tenant | null> {
    // Use mongoose as usual
    return TenantMongooseModel.findById(_id).lean().exec();
  }

  async create(data: NewTenantInput): Promise<Tenant> {
    const Tenant = new TenantMongooseModel(data);
    await Tenant.save();
    return Tenant;
  }
}
