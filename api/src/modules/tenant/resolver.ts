import { Resolver, Arg, Query, Mutation, ID } from "type-graphql";
import { Service } from "typedi";
import { ObjectId } from "mongodb";

import { Tenant } from "../../entities/Tenant";
import TenantService from "./service";
import { NewTenantInput } from "./input";

/*
  IMPORTANT: Your business logic must be in the service!
*/

@Service() // Dependencies injection
@Resolver((of) => Tenant)
export default class TenantResolver {
  constructor(private readonly TenantService: TenantService) {}

  @Query((returns) => Tenant)
  async getTenant(@Arg("id") id: ObjectId) {
    const Tenant = await this.TenantService.getById(id);

    return Tenant;
  }

  @Mutation((returns) => Tenant)
  async createTenant(
    @Arg("createTenantData") createTenantData: NewTenantInput
  ): Promise<Tenant> {
    const Tenant = await this.TenantService.addTenant(createTenantData);
    return Tenant;
  }
}