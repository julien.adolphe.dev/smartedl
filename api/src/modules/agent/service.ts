import { Service } from "typedi";
import { ObjectId } from "mongodb";

import AgentModel from "./model";
import { Agent } from "../../entities/Agent";
import { NewAgentInput } from "./input";

@Service() // Dependencies injection
export default class AgentService {
  constructor(private readonly AgentModel: AgentModel) {}

  public async getById(_id: ObjectId): Promise<Agent | null> {
    return this.AgentModel.getById(_id);
  }

  public async addAgent(data: NewAgentInput): Promise<Agent> {
    const newAgent = await this.AgentModel.create(data);

    // Business logic goes here
    // Example:
    // Trigger push notification, analytics, ...

    return newAgent;
  }
}
