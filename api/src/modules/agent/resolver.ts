import { Resolver, Arg, Query, Mutation, ID } from "type-graphql";
import { Service } from "typedi";
import { ObjectId } from "mongodb";

import { Agent } from "../../entities/Agent";
import AgentService from "./service";
import { NewAgentInput } from "./input";

/*
  IMPORTANT: Your business logic must be in the service!
*/

@Service() // Dependencies injection
@Resolver((of) => Agent)
export default class AgentResolver {
  constructor(private readonly AgentService: AgentService) {}

  @Query((returns) => Agent)
  async getAgent(@Arg("id") id: ObjectId) {
    const Agent = await this.AgentService.getById(id);

    return Agent;
  }

  @Mutation((returns) => Agent)
  async createAgent(
    @Arg("createAgentData") createAgentData: NewAgentInput
  ): Promise<Agent> {
    const Agent = await this.AgentService.addAgent(createAgentData);
    return Agent;
  }
}