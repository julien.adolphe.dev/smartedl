import { getModelForClass } from "@typegoose/typegoose";
import { ObjectId } from "mongodb";

import { Agent } from "../../entities/Agent";
import { NewAgentInput } from "./input";

// This generates the mongoose model for us
export const AgentMongooseModel = getModelForClass(Agent);

export default class AgentModel {
  async getById(_id: ObjectId): Promise<Agent | null> {
    // Use mongoose as usual
    return AgentMongooseModel.findById(_id).lean().exec();
  }

  async create(data: NewAgentInput): Promise<Agent> {
    const Agent = new AgentMongooseModel(data);
    await Agent.save();
    return Agent;
  }
}
