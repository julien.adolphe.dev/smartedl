import { getModelForClass } from "@typegoose/typegoose";
import { ObjectId } from "mongodb";

import { Inventory } from "../../entities/Inventory";
import { NewInventoryInput } from "./input";

// This generates the mongoose model for us
export const InventoryMongooseModel = getModelForClass(Inventory);

export default class InventoryModel {
  async getById(_id: ObjectId): Promise<Inventory | null> {
    // Use mongoose as usual
    return InventoryMongooseModel.findById(_id).lean().exec();
  }

  async create(data: NewInventoryInput): Promise<Inventory> {
    const Inventory = new InventoryMongooseModel(data);
    await Inventory.save();
    return Inventory;
  }
}
