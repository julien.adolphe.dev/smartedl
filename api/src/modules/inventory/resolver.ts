import { Resolver, Arg, Query, Mutation, ID } from "type-graphql";
import { Service } from "typedi";
import { ObjectId } from "mongodb";

import { Inventory } from "../../entities/Inventory";
import InventoryService from "./service";
import { NewInventoryInput } from "./input";

/*
  IMPORTANT: Your business logic must be in the service!
*/

@Service() // Dependencies injection
@Resolver((of) => Inventory)
export default class InventoryResolver {
  constructor(private readonly InventoryService: InventoryService) {}

  @Query((returns) => Inventory)
  async getInventory(@Arg("id") id: ObjectId) {
    const Inventory = await this.InventoryService.getById(id);

    return Inventory;
  }

  @Mutation((returns) => Inventory)
  async createInventory(
    @Arg("createInventoryData") createInventoryData: NewInventoryInput
  ): Promise<Inventory> {
    const Inventory = await this.InventoryService.addInventory(createInventoryData);
    return Inventory;
  }
}