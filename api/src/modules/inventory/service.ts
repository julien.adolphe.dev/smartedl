import { Service } from "typedi";
import { ObjectId } from "mongodb";

import InventoryModel from "./model";
import { Inventory } from "../../entities/Inventory";
import { NewInventoryInput } from "./input";

@Service() // Dependencies injection
export default class InventoryService {
  constructor(private readonly InventoryModel: InventoryModel) {}

  public async getById(_id: ObjectId): Promise<Inventory | null> {
    return this.InventoryModel.getById(_id);
  }

  public async addInventory(data: NewInventoryInput): Promise<Inventory> {
    const newInventory = await this.InventoryModel.create(data);

    // Business logic goes here
    // Example:
    // Trigger push notification, analytics, ...

    return newInventory;
  }
}
