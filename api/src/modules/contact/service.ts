import { Service } from "typedi";
import { ObjectId } from "mongodb";

import ContactModel from "./model";
import { Contact } from "../../entities/Contact";
import { NewContactInput } from "./input";

@Service() // Dependencies injection
export default class ContactService {
  constructor(private readonly ContactModel: ContactModel) {}

  public async getById(_id: ObjectId): Promise<Contact | null> {
    return this.ContactModel.getById(_id);
  }

  public async addContact(data: NewContactInput): Promise<Contact> {
    const newContact = await this.ContactModel.create(data);

    // Business logic goes here
    // Example:
    // Trigger push notification, analytics, ...

    return newContact;
  }
}
