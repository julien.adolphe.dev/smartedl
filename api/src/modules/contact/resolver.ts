import { Resolver, Arg, Query, Mutation, ID } from "type-graphql";
import { Service } from "typedi";
import { ObjectId } from "mongodb";

import { Contact } from "../../entities/Contact";
import ContactService from "./service";
import { NewContactInput } from "./input";

/*
  IMPORTANT: Your business logic must be in the service!
*/

@Service() // Dependencies injection
@Resolver((of) => Contact)
export default class ContactResolver {
  constructor(private readonly ContactService: ContactService) {}

  @Query((returns) => Contact)
  async getContact(@Arg("id") id: ObjectId) {
    const Contact = await this.ContactService.getById(id);

    return Contact;
  }

  @Mutation((returns) => Contact)
  async createContact(
    @Arg("createContactData") createContactData: NewContactInput
  ): Promise<Contact> {
    const Contact = await this.ContactService.addContact(createContactData);
    return Contact;
  }
}