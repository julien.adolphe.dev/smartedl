import { getModelForClass } from "@typegoose/typegoose";
import { ObjectId } from "mongodb";

import { Contact } from "../../entities/Contact";
import { NewContactInput } from "./input";

// This generates the mongoose model for us
export const ContactMongooseModel = getModelForClass(Contact);

export default class ContactModel {
  async getById(_id: ObjectId): Promise<Contact | null> {
    // Use mongoose as usual
    return ContactMongooseModel.findById(_id).lean().exec();
  }

  async create(data: NewContactInput): Promise<Contact> {
    const Contact = new ContactMongooseModel(data);
    await Contact.save();
    return Contact;
  }
}
