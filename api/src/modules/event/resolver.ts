import { Resolver, Arg, Query, Mutation, ID } from "type-graphql";
import { Service } from "typedi";
import { ObjectId } from "mongodb";

import { Event } from "../../entities/Event";
import EventService from "./service";
import { NewEventInput } from "./input";

/*
  IMPORTANT: Your business logic must be in the service!
*/

@Service() // Dependencies injection
@Resolver((of) => Event)
export default class EventResolver {
  constructor(private readonly EventService: EventService) {}

  @Query((returns) => Event)
  async getEvent(@Arg("id") id: ObjectId) {
    const Event = await this.EventService.getById(id);

    return Event;
  }

  @Mutation((returns) => Event)
  async createEvent(
    @Arg("createEventData") createEventData: NewEventInput
  ): Promise<Event> {
    const Event = await this.EventService.addEvent(createEventData);
    return Event;
  }
}