import { getModelForClass } from "@typegoose/typegoose";
import { ObjectId } from "mongodb";

import { Event } from "../../entities/Event";
import { NewEventInput } from "./input";

// This generates the mongoose model for us
export const EventMongooseModel = getModelForClass(Event);

export default class EventModel {
  async getById(_id: ObjectId): Promise<Event | null> {
    // Use mongoose as usual
    return EventMongooseModel.findById(_id).lean().exec();
  }

  async create(data: NewEventInput): Promise<Event> {
    const Event = new EventMongooseModel(data);
    await Event.save();
    return Event;
  }
}
