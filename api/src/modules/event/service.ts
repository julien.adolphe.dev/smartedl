import { Service } from "typedi";
import { ObjectId } from "mongodb";

import EventModel from "./model";
import { Event } from "../../entities/Event";
import { NewEventInput } from "./input";

@Service() // Dependencies injection
export default class EventService {
  constructor(private readonly EventModel: EventModel) {}

  public async getById(_id: ObjectId): Promise<Event | null> {
    return this.EventModel.getById(_id);
  }

  public async addEvent(data: NewEventInput): Promise<Event> {
    const newEvent = await this.EventModel.create(data);

    // Business logic goes here
    // Example:
    // Trigger push notification, analytics, ...

    return newEvent;
  }
}
