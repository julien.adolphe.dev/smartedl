import TodoResolver from "./todo/resolver";
import OwnerResolver from "./owner/resolver";
import AgentResolver from "./agent/resolver";
import ContactResolver from "./contact/resolver";
import ContractResolver from "./contract/resolver";
import EquipmentResolver from "./equipment/resolver";
import EventResolver from "./event/resolver";
import InventoryResolver from "./inventory/resolver";
import RoomResolver from "./room/resolver";
import TenantResolver from "./tenant/resolver";
import UserResolver from "./user/resolver";

// Important: Add all your module's resolver in this
export const resolvers: [Function, ...Function[]] = [
  TodoResolver,
  UserResolver,
  OwnerResolver,
  AgentResolver,
  ContactResolver,
  ContractResolver,
  EquipmentResolver,
  EventResolver,
  InventoryResolver,
  RoomResolver,
  TenantResolver,
  UserResolver
];
