import { getModelForClass } from "@typegoose/typegoose";
import { ObjectId } from "mongodb";

import { Equipment } from "../../entities/Equipment";
import { NewEquipmentInput } from "./input";

// This generates the mongoose model for us
export const EquipmentMongooseModel = getModelForClass(Equipment);

export default class EquipmentModel {
  async getById(_id: ObjectId): Promise<Equipment | null> {
    // Use mongoose as usual
    return EquipmentMongooseModel.findById(_id).lean().exec();
  }

  async create(data: NewEquipmentInput): Promise<Equipment> {
    const Equipment = new EquipmentMongooseModel(data);
    await Equipment.save();
    return Equipment;
  }
}
