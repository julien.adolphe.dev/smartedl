import { Service } from "typedi";
import { ObjectId } from "mongodb";

import EquipmentModel from "./model";
import { Equipment } from "../../entities/Equipment";
import { NewEquipmentInput } from "./input";

@Service() // Dependencies injection
export default class EquipmentService {
  constructor(private readonly EquipmentModel: EquipmentModel) {}

  public async getById(_id: ObjectId): Promise<Equipment | null> {
    return this.EquipmentModel.getById(_id);
  }

  public async addEquipment(data: NewEquipmentInput): Promise<Equipment> {
    const newEquipment = await this.EquipmentModel.create(data);

    // Business logic goes here
    // Example:
    // Trigger push notification, analytics, ...

    return newEquipment;
  }
}
