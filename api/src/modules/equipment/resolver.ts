import { Resolver, Arg, Query, Mutation, ID } from "type-graphql";
import { Service } from "typedi";
import { ObjectId } from "mongodb";

import { Equipment } from "../../entities/Equipment";
import EquipmentService from "./service";
import { NewEquipmentInput } from "./input";

/*
  IMPORTANT: Your business logic must be in the service!
*/

@Service() // Dependencies injection
@Resolver((of) => Equipment)
export default class EquipmentResolver {
  constructor(private readonly EquipmentService: EquipmentService) {}

  @Query((returns) => Equipment)
  async getEquipment(@Arg("id") id: ObjectId) {
    const Equipment = await this.EquipmentService.getById(id);

    return Equipment;
  }

  @Mutation((returns) => Equipment)
  async createEquipment(
    @Arg("createEquipmentData") createEquipmentData: NewEquipmentInput
  ): Promise<Equipment> {
    const Equipment = await this.EquipmentService.addEquipment(createEquipmentData);
    return Equipment;
  }
}