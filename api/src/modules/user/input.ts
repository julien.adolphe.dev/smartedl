import { Field, InputType, ID } from "type-graphql";
import { MaxLength, MinLength } from "class-validator";

@InputType()
export class NewUserInput {
  @Field()
  @MaxLength(300)
  @MinLength(1)
  firstname: String;

  @Field()
  @MaxLength(300)
  @MinLength(1)
  lastname: String;

  @Field()
  @MaxLength(300)
  @MinLength(1)
  company_name: String;

  @Field()
  @MaxLength(300)
  @MinLength(1)
  mail: String;

  @Field()
  @MaxLength(300)
  @MinLength(1)
  firebase_id: String
}
