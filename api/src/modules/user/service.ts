import { Service } from "typedi";
import { ObjectId } from "mongodb";

import UserModel from "./model";
import { User } from "../../entities/User";
import { NewUserInput } from "./input";

@Service() // Dependencies injection
export default class UserService {
  constructor(private readonly UserModel: UserModel) {}

  public async getById(_id: ObjectId): Promise<User | null> {
    return this.UserModel.getById(_id);
  }

  public async addUser(data: NewUserInput): Promise<User> {
    const newUser = await this.UserModel.create(data);

    // Business logic goes here
    // Example:
    // Trigger push notification, analytics, ...

    return newUser;
  }
}
