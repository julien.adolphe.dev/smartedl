import { Service } from "typedi";
import { ObjectId } from "mongodb";

import RoomModel from "./model";
import { Room } from "../../entities/Room";
import { NewRoomInput } from "./input";

@Service() // Dependencies injection
export default class RoomService {
  constructor(private readonly RoomModel: RoomModel) {}

  public async getById(_id: ObjectId): Promise<Room | null> {
    return this.RoomModel.getById(_id);
  }

  public async addRoom(data: NewRoomInput): Promise<Room> {
    const newRoom = await this.RoomModel.create(data);

    // Business logic goes here
    // Example:
    // Trigger push notification, analytics, ...

    return newRoom;
  }
}
