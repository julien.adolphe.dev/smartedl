import { getModelForClass } from "@typegoose/typegoose";
import { ObjectId } from "mongodb";

import { Room } from "../../entities/Room";
import { NewRoomInput } from "./input";

// This generates the mongoose model for us
export const RoomMongooseModel = getModelForClass(Room);

export default class RoomModel {
  async getById(_id: ObjectId): Promise<Room | null> {
    // Use mongoose as usual
    return RoomMongooseModel.findById(_id).lean().exec();
  }

  async create(data: NewRoomInput): Promise<Room> {
    const Room = new RoomMongooseModel(data);
    await Room.save();
    return Room;
  }
}
