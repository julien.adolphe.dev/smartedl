import { Resolver, Arg, Query, Mutation, ID } from "type-graphql";
import { Service } from "typedi";
import { ObjectId } from "mongodb";

import { Room } from "../../entities/Room";
import RoomService from "./service";
import { NewRoomInput } from "./input";

/*
  IMPORTANT: Your business logic must be in the service!
*/

@Service() // Dependencies injection
@Resolver((of) => Room)
export default class RoomResolver {
  constructor(private readonly RoomService: RoomService) {}

  @Query((returns) => Room)
  async getRoom(@Arg("id") id: ObjectId) {
    const Room = await this.RoomService.getById(id);

    return Room;
  }

  @Mutation((returns) => Room)
  async createRoom(
    @Arg("createRoomData") createRoomData: NewRoomInput
  ): Promise<Room> {
    const Room = await this.RoomService.addRoom(createRoomData);
    return Room;
  }
}