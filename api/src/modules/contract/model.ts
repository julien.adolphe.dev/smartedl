import { getModelForClass } from "@typegoose/typegoose";
import { ObjectId } from "mongodb";

import { Contract } from "../../entities/Contract";
import { NewContractInput } from "./input";

// This generates the mongoose model for us
export const ContractMongooseModel = getModelForClass(Contract);

export default class ContractModel {
  async getById(_id: ObjectId): Promise<Contract | null> {
    // Use mongoose as usual
    return ContractMongooseModel.findById(_id).lean().exec();
  }

  async create(data: NewContractInput): Promise<Contract> {
    const Contract = new ContractMongooseModel(data);
    await Contract.save();
    return Contract;
  }
}
