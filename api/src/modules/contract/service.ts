import { Service } from "typedi";
import { ObjectId } from "mongodb";

import ContractModel from "./model";
import { Contract } from "../../entities/Contract";
import { NewContractInput } from "./input";

@Service() // Dependencies injection
export default class ContractService {
  constructor(private readonly ContractModel: ContractModel) {}

  public async getById(_id: ObjectId): Promise<Contract | null> {
    return this.ContractModel.getById(_id);
  }

  public async addContract(data: NewContractInput): Promise<Contract> {
    const newContract = await this.ContractModel.create(data);

    // Business logic goes here
    // Example:
    // Trigger push notification, analytics, ...

    return newContract;
  }
}
