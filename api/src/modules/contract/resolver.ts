import { Resolver, Arg, Query, Mutation, ID } from "type-graphql";
import { Service } from "typedi";
import { ObjectId } from "mongodb";

import { Contract } from "../../entities/Contract";
import ContractService from "./service";
import { NewContractInput } from "./input";

/*
  IMPORTANT: Your business logic must be in the service!
*/

@Service() // Dependencies injection
@Resolver((of) => Contract)
export default class ContractResolver {
  constructor(private readonly ContractService: ContractService) {}

  @Query((returns) => Contract)
  async getContract(@Arg("id") id: ObjectId) {
    const Contract = await this.ContractService.getById(id);

    return Contract;
  }

  @Mutation((returns) => Contract)
  async createContract(
    @Arg("createContractData") createContractData: NewContractInput
  ): Promise<Contract> {
    const Contract = await this.ContractService.addContract(createContractData);
    return Contract;
  }
}