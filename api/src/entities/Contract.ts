import { ObjectType, Field , ID} from 'type-graphql';
import { prop as Property, getModelForClass } from "@typegoose/typegoose";
import { ObjectId } from 'mongodb';


@ObjectType({ description : "The Contract Model"})
export class Contract {

    @Field(() => ID)
    _id: ObjectId;

    @Field()
    @Property({ required: false })
    warranty_deposit: Number

    @Field()
    @Property({ required: false })
    house_insurance_name: String

    @Field()
    @Property({ default: new Date(), required: false, nullable: true })
    insurance_effective_date: Date

    @Field()
    @Property({ required: false })
    contract_photos: String

    @Field()
    @Property({ required: false })
    sweeping_date: Date

    @Field()
    @Property({ default: new Date(), required: false, nullable: true })
    boiler_maintenance_date: Date

    @Field()
    @Property({ required: false })
    comment: String

    @Field()
    @Property({ required: false })
    key_photo: String

    @Field()
    @Property({ required: false })
    inventory_pdf: String
}

export const ContractModel = getModelForClass(Contract);