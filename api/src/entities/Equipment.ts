import { ObjectType, Field , ID} from 'type-graphql';
import { prop as Property, getModelForClass } from "@typegoose/typegoose";
import { ObjectId } from 'mongodb';
import { Contact } from './Contact';



@ObjectType({ description : "The Equipment Model"})
export class Equipment {

    @Field(() => ID)
    _id: ObjectId;

    @Field()
    name: String

    @Field()
    state: String

    @Field()
    nature: String

    @Field()
    color: String

    @Field()
    observation: String

    @Field()
    comment: String

    @Field()
    photo: String 

}

export const EquipmentModel = getModelForClass(Equipment);
