import { ObjectType, Field , ID} from 'type-graphql';
import { prop , getModelForClass } from "@typegoose/typegoose";
import { Ref } from "../types";
import { ObjectId } from 'mongodb';
import { Owner } from './Owner';
import { Tenant } from './Tenant';
import { Agent } from './Agent';
import { Contract } from './Contract';
import { Property } from './Property';
import { Room } from './Room';



@ObjectType({ description : "The Inventory Model"})
export class Inventory {

    @Field((_type: any) => String)
    @prop({ ref: Owner, required: true})
    owners: Ref<Owner>;
    _doc_owner: any

    @Field((_type: any) => String)
    @prop({ ref: Tenant, required: true})
    tenants: Ref<Tenant>;
    _doc_tenant: any

    @Field((_type: any) => String)
    @prop({ ref: Agent, required: true})
    agents: Ref<Agent>;
    _doc_agent: any

    @Field((_type: any) => String)
    @prop({ ref: Contract, required: true})
    contract: Ref<Contract>;
    _doc_contract: any

    @Field((_type: any) => String)
    @prop({ ref: Property, required: true})
    properties: Ref<Property>;
    _doc_property: any

    @Field((_type: any) => String)
    @prop({ ref: Room, required: true})
    rooms: Ref<Room>;
    _doc_room: any


}

export const InventoryModel = getModelForClass(Inventory);