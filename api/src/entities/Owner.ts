import { ObjectType, Field , ID} from 'type-graphql';
import { prop as Property, getModelForClass } from "@typegoose/typegoose";
import { ObjectId } from 'mongodb';
import { Contact } from './Contact';



@ObjectType({ description : "The Owner Model"})
export class Owner {

    @Field(() => ID)
    _id: ObjectId;

    @Field()
    @Property({ required: true })
    name: String

    @Field()
    @Property({ required: true })
    surname: String

    @Field()
    @Property({ required: false })
    company_name: String

    @Field()
    @Property({ required: true })
    address_1: String

    @Field()
    @Property({ required: false })
    address_2: String

    @Field()
    @Property({ required: true })
    phone_number: String

    @Field()
    @Property({ required: true })
    mail: String

    @Field()
    @Property({ required: true })
    zip_code: String

    @Field()
    @Property({ required: true })
    city: String

    @Field()
    @Property({ required: true })
    ref_housing: String

    @Field()
    @Property({ required: true })
    signature: String
}

export const OwnerModel = getModelForClass(Owner);