import { ObjectType, Field , ID} from 'type-graphql';
import { prop, getModelForClass } from "@typegoose/typegoose";
import { ObjectId } from 'mongodb';
import { Owner } from './Owner';
import { Ref } from '../types'


@ObjectType({ description : "The Property Model"})
export class Property {

    @Field(() => ID)
    _id: ObjectId;

    @Field()
    @prop({ required: true })
    reference: string

    @Field()
    @prop({ required: true })
    area: number

    @Field()
    @prop({ required: true })
    address_1: String

    @Field()
    @prop({ required: false })
    address_2: String

    @Field()
    @prop({ required: true })
    zip_code: String

    @Field()
    @prop({ required: true })
    city: String

    @Field()
    @prop({ required: true })
    cellar: String

    @Field()
    @prop({ required: false })
    floor: Number

    @Field()
    @prop({ required: false })
    door: String

    @Field()
    @prop({ required: false })
    building_code: Number

    @Field()
    @prop({ required: true })
    floor_number: Number

    @Field()
    @prop({ required: false })
    mailbox: String

    @Field()
    @prop({ required: true })
    parking_spot: String

    @Field()
    @prop({ required: false })
    comment: String

    //@Field((_type: any) => String)
    @prop({ ref: Owner, required: true})
    owners: Ref<Owner>;
    _doc_owner: any

    @Field()
    @prop({ default: new Date(), required: false, nullable: true })
    date: Date
    
}

export const PropertyModel = getModelForClass(Property);