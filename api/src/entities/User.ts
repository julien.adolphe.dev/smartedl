import { ObjectType , Field } from "type-graphql"
import { prop , getModelForClass} from "@typegoose/typegoose"
import { ObjectId } from 'mongodb';


@ObjectType({ description: "The user model"})
export class User {
    @Field()
    readonly _id!: ObjectId;

    @prop({ required: true })
    @Field()
    firstname!: String;

    @prop({ required: true })
    @Field()
    lastname!: String;

    @prop({ required: true })
    @Field()
    company_name!: String

    @prop({ required: true })
    @Field()
    mail!: String

    @prop()
    @Field()
    firebase_id!: String
}

export const UserModel = getModelForClass(User);