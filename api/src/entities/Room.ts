import { ObjectType, Field , ID} from 'type-graphql';
import { prop as Property, getModelForClass } from "@typegoose/typegoose";
import { ObjectId } from 'mongodb';
import { Equipment } from './Equipment';
import { Ref } from '../types';



@ObjectType({ description : "The Room Model"})
export class Room {

    @Field(() => ID)
    _id: ObjectId;

    @Field()
    name: String

    @Field()
    note: String


    @Field()
    element: Number

    
    //@Field((_type: any) => String)
    @Property({ ref: Room, required: true})
    rooms: Ref<Room>;
    _doc_room: any


}


export const RoomModel = getModelForClass(Room);