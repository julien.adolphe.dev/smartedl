import { ObjectType, Field , ID} from 'type-graphql';
import { prop as Property, getModelForClass } from "@typegoose/typegoose";
import { ObjectId } from 'mongodb';
import { Contact } from './Contact';
import { Ref } from '../types'



@ObjectType({ description : "The Event Model"})
export class Event {

    @Field(() => ID)
    _id: ObjectId;

    @Field()
    @Property({ required: true })
    name: String

    @Field()
    @Property({ required: true })
    address: String

    @Field()
    @Property({ required: true })
    zip_code: String

    @Field()
    @Property({ required: true })
    city: String

    //@Field((_type: any) => String)
    @Property({ ref: Contact, required: false})
    rooms: Ref<Contact>;
    _doc_room: any

    @Field()
    @Property({ required: false })
    comment: String

    @Field()
    @Property({ default: new Date(), required: true, nullable: true })
    created_at: Date

    @Field()
    @Property({ default: new Date(), required: true, nullable: true })
    updated_at: Date


}

export const EventModel = getModelForClass(Event);