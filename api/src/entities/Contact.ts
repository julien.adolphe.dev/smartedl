import { ObjectType, Field , ID} from 'type-graphql';
import { prop as Property, getModelForClass } from "@typegoose/typegoose";
import { ObjectId } from 'mongodb';


@ObjectType({ description : "The Contact Model"})
export class Contact {

    @Field(() => ID)
    _id: ObjectId;

    @Field()
    @Property({ required: true })
    name: String

    @Field()
    @Property({ required: true })
    surname: String

    @Property({ required: false })
    @Field()
    company_name: String

    @Property({ required: true })
    @Field()
    address_1: String

    @Property({ required: false })
    @Field()
    address_2: String

    @Property({ required: true })
    @Field()
    phone_number: Number

    @Property({ required: true })
    @Field()
    mail: String

    @Property({ required: true })
    @Field()
    zip_code: Number

    @Property({ required: true })
    @Field()
    city: String
    
    @Property({ required: false })
    @Field()
    ref_housing: String

}

export const ContactModel = getModelForClass(Contact);